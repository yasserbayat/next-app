import {shuffleArray} from './utils'

export const Difficulty = {
  EASY: 'easy',
  MEDIUM: 'medium',
  HARD: 'hard',
}

export const fetchQuestions = async (amount, difficulty) => {
  const endpoint = `https://opentdb.com/api.php?amount=${ amount }&difficulty=${ difficulty }&type=multiple`

  try {
    const data = await (await fetch(endpoint)).json()
    return data.results.map(result => {
      return {
        ...result,
        answers: shuffleArray([ ...result.incorrect_answers, result.correct_answer ]),
      }
    })

  } catch (e) {
    console.log(e)
  }

}