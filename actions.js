export const actionTypes = {
  FAILURE: 'FAILURE',
  INCREMENT: 'INCREMENT',
  DECREMENT: 'DECREMENT',
  RESET: 'RESET',
  LOAD_DATA: 'LOAD_DATA',
  LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
  GET_ARTISTS: 'GET_ARTISTS',
  GET_ARTISTS_SUCCESS: 'GET_ARTISTS_SUCCESS',
  GET_ARTISTS_FAILED: 'GET_ARTISTS_FAILED',
  GET_NOTES: 'GET_NOTES',
  GET_NOTES_SUCCESS: 'GET_NOTES_SUCCESS',
  GET_NOTES_FAILED: 'GET_NOTES_FAILED',
  START_CLOCK: 'START_CLOCK',
  TICK_CLOCK: 'TICK_CLOCK',
  HYDRATE: 'HYDRATE',
}

export function failure(error) {
  return {
    type: actionTypes.FAILURE,
    error,
  }
}

export function increment() {
  return {type: actionTypes.INCREMENT}
}

export function decrement() {
  return {type: actionTypes.DECREMENT}
}

export function reset() {
  return {type: actionTypes.RESET}
}

export function loadData() {
  return {type: actionTypes.LOAD_DATA}
}

export function loadDataSuccess(data) {
  return {
    type: actionTypes.LOAD_DATA_SUCCESS,
    data,
  }
}

export function startClock() {
  return {type: actionTypes.START_CLOCK}
}

export function tickClock(isServer) {
  return {
    type: actionTypes.TICK_CLOCK,
    light: !isServer,
    ts: Date.now(),
  }
}

//Artists action creators
export function getArtists() {
  return {
    type: actionTypes.GET_ARTISTS,
  }
}

export function getArtistsSuccess(data) {
  return {
    type: actionTypes.GET_ARTISTS_SUCCESS,
    data,
  }
}

export function getArtistsFailed(error) {
  return {
    type: actionTypes.GET_ARTISTS_FAILED,
    error,
  }
}

//Notes Action creators
export function getNotes() {
  return {
    type: actionTypes.GET_NOTES,
  }
}

export function getNotesSuccess(data) {
  return {
    type: actionTypes.GET_NOTES_SUCCESS,
    data,
  }
}

export function getNotesFailed(error) {
  return {
    type: actionTypes.GET_NOTES_FAILED,
    action: {error},
  }
}