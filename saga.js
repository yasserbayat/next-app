import {all, call, delay, put, take, takeLatest} from 'redux-saga/effects'
import {
  actionTypes,
  failure, getArtistsFailed,
  getArtistsSuccess, getNotesFailed, getNotesSuccess,
  loadDataSuccess,
  tickClock,
} from './actions'

function* runClockSaga() {
  yield take(actionTypes.START_CLOCK)
  while (true) {
    yield put(tickClock(false))
    yield delay(1000)
  }
}

function* loadDataSaga() {
  try {
    const res = yield fetch('https://jsonplaceholder.typicode.com/users')
    const data = yield res.json()
    yield put(loadDataSuccess(data))
  } catch (err) {
    yield put(failure(err))
  }
}

function* getArtistsSaga() {
  try {
    const res = yield fetch('https://vocadb.net/api/artists')
    const data = yield res.json()
    yield put(getArtistsSuccess(data))
  } catch (err) {
    yield put(getArtistsFailed(err))
  }

}

function* getNotesSaga() {
  try {
    const res = yield fetch('http://localhost:3000/notes', {
      mode: 'cors',
      credentials: 'include',
    })
    const data = yield res.json()
    yield put(getNotesSuccess(data))
  } catch (e) {
    put(getNotesFailed(e))
  }
}


/**
 *
 * WATCHER
 *
 */
function* rootSaga() {
  yield all([
    call(runClockSaga),
    takeLatest(actionTypes.LOAD_DATA, loadDataSaga),
    takeLatest(actionTypes.GET_ARTISTS, getArtistsSaga),
    takeLatest(actionTypes.GET_NOTES, getNotesSaga),
  ])
}

export default rootSaga
