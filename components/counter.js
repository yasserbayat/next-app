import React from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {increment, decrement, reset} from '../actions'

const Counter = () => {
  const dispatch = useDispatch()
  const count = useSelector((state) => state && state.count)

  return (
    <div>
      <style jsx>{ `
        div {
          padding: 0 0 20px 0;
        }
      ` }</style>
      <h1>
        Count: <span>{ count ?? count }</span>
      </h1>
      <button onClick={ () => dispatch(increment()) }>+1</button>
      <button onClick={ () => dispatch(decrement()) }>-1</button>
      <button onClick={ () => dispatch(reset()) }>Reset</button>
    </div>
  )
}

export default Counter
