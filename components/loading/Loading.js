import React from 'react'
import styles from './Loading.module.css'

const Loading = ({height, width}) => (
  <div className={ styles.loading } style={{height: `${height}px`, width:`${width}px`}}/>
)

export default Loading