import React, {useState} from 'react'


const QuizResult = ({userAnswers, totalNum}) => {
  const [ num, setNum ] = useState(0)

  const nextAnswer = () => {
    setNum(prev => prev + 1)
  }

  return (
    <>
      <div className="result-wrapper">
        { userAnswers.map((item, index) => (
            index === num && <div key={ index } className="question-card">
              <p className="question-number">Answer { num + 1 } / { totalNum }</p>
              <p className="question" dangerouslySetInnerHTML={ {__html: 'Question: ' + item.question} }/>
              <p className={ `answer ${ item.correct ? 'is-correct' : 'is-false' }` }
                 dangerouslySetInnerHTML={ {__html: item.answer} }/>
              <p className="correct">{ item.correctAnswer }</p>
            </div>
          ),
        ) }
      </div>
      { num < totalNum - 1 && (
        <button onClick={ nextAnswer }>Next</button>
      ) }

    </>
  )
}

export default QuizResult