import React from 'react'


const QuestionCard = ({questionNum, totalQuestions, question, answers, userAnswer, callback}) => {

  const setClassName = (correct, userClicked) => {
    if (correct) {
      return 'is-correct'
    } else if (!correct && userClicked) {
      return 'is-false'
    } else {
      return 'is-disabled'
    }
  }
  return (
    <div className="question-card">
      <p className="question-number">Question: { questionNum } / { totalQuestions }</p>
      <p className="question" dangerouslySetInnerHTML={ {__html: question} }/>
      <div>
        {
          answers.map(answer => (
            <div key={ answer } className="button-wrapper">
              <button
                className={ setClassName(userAnswer?.correctAnswer === answer, userAnswer?.answer === answer) }
                disabled={ userAnswer ? userAnswer : undefined }
                onClick={ callback }
                value={ answer }
                dangerouslySetInnerHTML={ {__html: answer} }
              />
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default QuestionCard