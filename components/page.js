import React from 'react'
import Link from 'next/link'
import {useSelector} from 'react-redux'

import Counter from './counter'
import Clock from './clock'

function Page({linkTo, NavigateTo, title}) {
  const error = useSelector((state) => state.error)
  const light = useSelector((state) => state.light)
  const lastUpdate = useSelector((state) => state.lastUpdate)
  return (
    <div>
      <h1>{ title }</h1>
      <Clock lastUpdate={ lastUpdate } light={ light }/>
      <Counter/>
      <nav>
        <Link href={ linkTo }>
          <a>Navigate: { NavigateTo }</a>
        </Link>
      </nav>
      { error && <p style={ {color: 'red'} }>Error: { error.message }</p> }
    </div>
  )
}

export default Page
