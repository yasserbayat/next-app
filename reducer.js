import {actionTypes} from './actions'
import {HYDRATE} from 'next-redux-wrapper'

const initialState = {
  count: 0,
  error: false,
  lastUpdate: 0,
  light: false,
  placeholderData: null,
  artists: null,
  notes: [],
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case HYDRATE: {
      return {...state, ...action.payload}
    }

    case actionTypes.FAILURE:
      return {
        ...state,
        ...{error: action.error},
      }

    case actionTypes.INCREMENT:
      return {
        ...state,
        ...{count: state.count + 1},
      }

    case actionTypes.DECREMENT:
      return {
        ...state,
        ...{count: state.count - 1},
      }

    case actionTypes.RESET:
      return {
        ...state,
        ...{count: initialState.count},
      }

    case actionTypes.LOAD_DATA_SUCCESS:
      return {
        ...state,
        ...{placeholderData: action.data},
      }

    case actionTypes.TICK_CLOCK:
      return {
        ...state,
        ...{lastUpdate: action.ts, light: !!action.light},
      }

    case actionTypes.GET_ARTISTS_SUCCESS:
      return {
        ...state,
        ...{artists: action.data.items || {}},
      }

    case actionTypes.GET_ARTISTS_FAILED:
      return {
        ...state,
        ...{error: action.error},
      }

    case actionTypes.GET_NOTES_SUCCESS:
      return {
        ...state,
        ...{notes: action.data},
      }

    case actionTypes.GET_NOTES_FAILED:
      return {
        ...state,
        ...{error: action.error},
      }

    default:
      break
  }
}

export default reducer