import React, {useRef, useState} from 'react'

const Form = () => {
  const refs = {
    form: useRef(null),
    step1: useRef(null),
    step2: useRef(null),
    step3: useRef(null),
    step4: useRef(null),
  }

  const [ validate, setValidate ] = useState(true)
  const [ formStep, setFormStep ] = useState(1)

  const handleSubmit = e => {
    e.preventDefault()
    e.stopPropagation()

    if (validation()) {
      refs.form.current.submit()
    }
  }

  const handleChange = e => e.currentTarget.classList.remove('invalid')

  const handleClick = (e) => {
    const {id} = e.currentTarget

    if (formStep <= 1) {
      setFormStep(1)
    }

    if (id === 'nextBtn' && formStep < 4 && validation()) {
      setFormStep(formStep + 1)
      goTOStep(formStep + 1)

    }

    if (id === 'prevBtn' && formStep > 1) {
      setFormStep(formStep - 1)
      goTOStep(formStep - 1)
    }

  }

  const goTOStep = n => {
    Object.keys(refs).forEach(ref => {
      const step = refs[ref]

      if (step === refs[`step${ n }`]) {
        step.current && step.current.classList.add('active')
      } else {
        step.current && step.current.classList.remove('active')
      }
    })
  }

  const showButtons = () => {
    return (
      formStep === 1 ?
        <button type="button" id="nextBtn" onClick={ handleClick }>Next</button>
        :
        formStep > 1 && formStep < 4 ?
          (
            <>
              <button type="button" id="prevBtn" onClick={ handleClick }>Previous</button>
              <button type="button" id="nextBtn" onClick={ handleClick }>Next</button>
            </>
          ) : formStep >= 4 &&
          <>
            <button type="button" id="prevBtn" onClick={ handleClick }>Previous</button>
            <button type="button" id="submitBtn" onClick={ handleSubmit }>Submit</button>
          </>
    )
  }

  const validation = () => {
    const form = refs.form.current
    const tabs = form.querySelectorAll('.tab')
    const inputs = tabs[formStep - 1].getElementsByTagName('input') //indexes begin from 0
    let i, valid = true

    for (i = 0; i < inputs.length; i++) {
      if (inputs[i].value === '') {
        inputs[i].classList.add('invalid')
        valid = false
      }
    }

    if (valid) {
      refs[`step${ formStep }`].current.className += ' finish'
    }
    setValidate(valid)
    return valid
  }

  const inputDisplay = n => {
    return {display: `${ formStep === n ? 'flex' : 'none' }`}
  }

  return (
    <form id="regForm" ref={ refs.form }>

      <h1>Register:</h1>

      {/*One "tab" for each step in the form: */ }
      <div className="tab" style={ inputDisplay(1) }>Name:
        <p><input required placeholder="First name..." onChange={ handleChange }/></p>
        <p><input required placeholder="Last name..." onChange={ handleChange }/></p>
      </div>

      <div className="tab" style={ inputDisplay(2) }>Contact Info:
        <p><input required placeholder="E-mail..." onChange={ handleChange }/></p>
        <p><input required placeholder="Phone..." onChange={ handleChange }/></p>
      </div>

      <div className="tab" style={ inputDisplay(3) }>Birthday:
        <p><input required placeholder="dd" onChange={ handleChange }/></p>
        <p><input required placeholder="mm" onChange={ handleChange }/></p>
        <p><input required placeholder="yyyy" onChange={ handleChange }/></p>
      </div>

      <div className="tab" style={ inputDisplay(4) }>Login Info:
        <p><input required placeholder="Username..." onChange={ handleChange }/></p>
        <p><input required placeholder="Password..." onChange={ handleChange }/></p>
      </div>

      <span className={ `error${ !validate ? ' show' : '' }` }>Please fill out the required fields</span>

      <div style={ {overflow: 'auto'} }>
        <div className="buttons">
          { showButtons() }

        </div>
      </div>

      {/*Circles which indicates the steps of the form: */ }
      <div className="steps">
        <span className="step step1 active" ref={ refs.step1 }/>
        <span className="step step2" ref={ refs.step2 }/>
        <span className="step step3" ref={ refs.step3 }/>
        <span className="step step4" ref={ refs.step4 }/>
      </div>


    </form>
  )
}

export default Form