import React from 'react'
import { wrapper } from '../store'
import Head from 'next/head';
import '../public/css/main.css'


function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Catamaran:wght@600&family=Fascinate+Inline&display=swap"
          rel="stylesheet"
          key="google-font-cabin"
        />
      </Head>
    <Component {...pageProps} />
    </>
    )
}

export default wrapper.withRedux(App)