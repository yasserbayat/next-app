import Link from 'next/link'
import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {END} from 'redux-saga'
import {loadData} from '../actions'
import {NextSeo} from 'next-seo'
import {makeStyles} from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Loading from '../components/loading/Loading'

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    width: '100%',
    minWidth: 300,
    margin: 10,
    borderRadius: 0,
  },

  container: {
    padding: 10,
    display: 'flex',
    justifyContent: 'center',
    flexFlow: 'row wrap',
  },
})

const Users = () => {
  const classes = useStyles()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadData())
  }, [ dispatch ])

  const users = useSelector((state) => state && state.placeholderData)

  return (
    <>
      <NextSeo
        title='Users Info'
        description="This is Users Page Description"
        keywords='users, user, site'
      />
      <h1>Users</h1>
      <div className={ classes.container }>
        { !users ?
          <Loading/>
          :
          users.map((user, index) => (
            <Card key={ Math.random() + index } className={ classes.root }>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt={ `${ user.name }` }
                  height="140"
                  image={ `${ user.picture ? user.picture : '/img/user.svg' }` }
                  title={ `${ user.name }` }
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    <small>Name:</small> { user.name }
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Username: { user.username }
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Share
                </Button>
                <Button size="small" color="primary">
                  <Link href={ `/profile/${ user.id }` }><a target="_blank">Learn More</a></Link>
                </Button>
              </CardActions>
            </Card>
          ))
        }
      </div>
    </>
  )
}


export default Users