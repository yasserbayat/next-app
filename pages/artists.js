import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {getArtists} from '../actions'
import SearchIcon from '@material-ui/icons/Search'
import Loading from '../components/loading/Loading'


const Artists = () => {
  const dispatch = useDispatch()
  const artists = useSelector(state => state && state.artists)

  const [ filtered, setFiltered ] = useState([])
  const [ searchState, setSearchState ] = useState(false)

  useEffect(() => {
    dispatch(getArtists())
  }, [ dispatch ])

  function handleChange(e) {
    const val = e.currentTarget.value
    const newArr = artists.filter(item => item.artistType.toLowerCase().indexOf(val) !== -1)
    setFiltered(newArr)
    setSearchState(true)
  }

  const renderResults = (arr) => {
    return (
      !arr ?
        <Loading height={ 50 } width={ 50 }/> :
        arr.length === 0 && searchState ?
          <div className="results__inner">
            <h4>No match found</h4>
          </div>
          :
          arr.map(artist => (
              <div key={ artist.id } className="results__inner">
                <h4>Artist Type: { artist.artistType }</h4>
                <h5>Artist name: { artist.defaultName }</h5>
                <h5>Language: { artist.defaultNameLanguage }</h5>
              </div>
            ),
          )
    )
  }

  return (
    <div className="container">
      <h1>Artists</h1>
      <form className="filter-artists">
        <div className="form-control">
          <input type="text" name="searchArtists" id="searchArtists" onChange={ handleChange }/>
          <button className="search-btn">
            <SearchIcon/>
          </button>
        </div>
        <div className="results">
          { renderResults(filtered.length === 0 && !searchState ? artists : filtered) }
        </div>
      </form>
    </div>
  )
}


export default Artists