import React from 'react'
import Link from 'next/link'
import {useEffect} from 'react'
import {useDispatch} from 'react-redux'
import {END} from 'redux-saga'
import {wrapper} from '../store'
import {loadData, startClock, tickClock} from '../actions'
import Page from '../components/page'

const Index = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(startClock())
  }, [ dispatch ])

  return (
    <>
      <nav>
        <Link href="/users"><a href="/users">Users</a></Link>
        <Link href="/form/form"><a href="/form/form">Register</a></Link>
        <Link href="/quiz"><a href="/quiz">Quiz</a></Link>
      </nav>
      <Page title="Index Page" linkTo="/other" NavigateTo="Other Page"/>
    </>
  )
}

export const getStaticProps = wrapper.getStaticProps(async ({store}) => {
  store.dispatch(tickClock(false))

  if (!store.getState().placeholderData) {
    store.dispatch(loadData())
    store.dispatch(END)
  }

  await store.sagaTask.toPromise()
})

export default Index
