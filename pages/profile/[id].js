import {NextSeo} from 'next-seo'
import {useRouter} from 'next/router'
import React, {useEffect} from 'react'
import {shallowEqual, useDispatch, useSelector} from 'react-redux'
import {loadData} from '../../actions'
import Loading from '../../components/loading/Loading'


const User = () => {
  const dispatch = useDispatch()

  useEffect(() => dispatch(loadData()), [ dispatch ])

  const router = useRouter()
  const id = router.query.id

  const users = useSelector((state) => state && state.placeholderData, shallowEqual)
  const user = users && users.filter(user => user.id === +id)[0]

  return (
    <div className='user-profile'>
      <NextSeo
        title={ `User Profile/${ user ? user.name : '' }` }
        description="This is User Profile Page Description"
        keywords='users, user, site'
      />

      { !user ?
        <Loading/>
        :
        <h1>Hello { user.name }</h1>
      }

    </div>
  )
}

export default User