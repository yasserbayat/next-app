import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {getNotes} from '../actions'
import Loading from '../components/loading/Loading'


const Messages = () => {
  const dispatch = useDispatch()
  const [ show, setShow ] = useState(false)

  useEffect(() => {
    dispatch(getNotes())
  }, [ dispatch ])

  const notes = useSelector(state => state && state.notes)

  const handleClick = () => {
    setShow(true)
  }

  return (
    <div className="messages">
      <h1>Messages</h1>
      <button onClick={ handleClick }>Get Messages</button>
      <div className="messages">
        { show && !notes ?
          <Loading/> :
          show && notes && notes.map(note => <h4>{ note.text }</h4>)
        }
      </div>
    </div>
  )
}

export default Messages
