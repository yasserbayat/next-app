import React, {useState} from 'react'
import Loading from '../components/loading/Loading'
import {fetchQuestions, Difficulty} from '../public/quizUtils/API'
import QuestionCard from '../components/QuestionCard'
import QuizResult from '../components/QuizResult'

const TOTAL_QUESTIONS = 10

const Quiz = () => {
  const [ number, setNumber ] = useState(0)
  const [ loading, setLoading ] = useState(false)
  const [ questions, setQuestions ] = useState([])
  const [ userAnswers, setUserAnswers ] = useState([])
  const [ score, setScore ] = useState(0)
  const [ gameOver, setGameOver ] = useState(true)
  const [ show, setShow ] = useState(false)

  const startQuiz = async () => {
    setLoading(true)
    setGameOver(false)

    const newQuestions = await fetchQuestions(TOTAL_QUESTIONS, Difficulty.EASY)
    setQuestions(newQuestions)
    setScore(0)
    setUserAnswers([])
    setNumber(0)
    setLoading(false)
    setShow(false)
  }

  const checkAnswer = e => {
    if (!gameOver) {
      const answer = e.currentTarget.value
      const correct = questions[number].correct_answer === answer
      if (correct) setScore(prev => prev + 1)

      const answerObject = {
        question: questions[number].question,
        answer,
        correct,
        correctAnswer: questions[number].correct_answer,
      }

      setUserAnswers(prev => [ ...prev, answerObject ])
    }
  }

  const nextQuestion = () => {
    const nextQuestion = number + 1
    if (nextQuestion === TOTAL_QUESTIONS) {
      setGameOver(true)
    } else {
      setNumber(nextQuestion)
    }

  }

  const showResult = () => {
    setShow(true)
  }

  return (
    <div className="quiz-wrapper">
      <h1>Quiz</h1>
      { gameOver || userAnswers.length === TOTAL_QUESTIONS ?
        <button className="start" onClick={ startQuiz }>Start</button>
        : null }
      { !gameOver && <p className="score">Score: { score }</p> }
      { loading && <Loading height={ 50 } width={ 50 }/> }
      { !loading && !gameOver && (
        <QuestionCard
          questionNum={ number + 1 }
          totalQuestions={ TOTAL_QUESTIONS }
          question={ questions[number].question }
          answers={ questions[number].answers }
          userAnswer={ userAnswers ? userAnswers[number] : undefined }
          callback={ checkAnswer }
        />
      ) }

      { !loading && gameOver && userAnswers.length !== 0 && !show && (
        <button className="show-result" onClick={ showResult }>Show result</button>
      ) }

      { show && gameOver && !loading && (
        <QuizResult userAnswers={ userAnswers } totalNum={ TOTAL_QUESTIONS }/>
      ) }

      { !gameOver && !loading && userAnswers.length === number + 1 && (
        <button className="next" onClick={ nextQuestion }>{
          number !== TOTAL_QUESTIONS - 1 ? 'Next Question' : 'End' }</button>
      ) }
    </div>
  )
}

export default Quiz